const RedisService = require('../redis');


//Проверка на валидность access token
async function authMiddleware(req, res, next) {
    try {
        if (req.headers.authorization) {
            console.log(req.headers.authorization)
            let accessToken = req.headers.authorization.substr(7, req.headers.authorization.length);
            let ipAddr = await RedisService.aget(accessToken);
            if (ipAddr) {
                next();
            } else {
                res.status(400).json({ error: "Access token is expired, need to update or create" })
            }
        } else {
            res.status(400).json({ error: "Access token is empty" })
        }
    } catch (error) {
        res.status(500).json(error);
    }

}

module.exports = authMiddleware;