var validate = require("validate.js");
//Создание кастомного валидатора
validate.validators.currency = function (value) {
    if (value) {
        let cur = value.split(',');
        const defaultCur = ['RUB', 'EUR', 'USD', 'JPY'];
        if ((cur.some(_ => defaultCur.includes(_))) && (cur.length <= defaultCur.length)) {
            return
        }
        return "is must to be in ['RUB', 'EUR', 'USD', 'JPY']";
    }
    return
};
//Валидация переменных запроса с помощью библиотеки validate.js
function queryValidate(req, res, next) {
    try {
        let validationResult = validate(req.query, req.queryConstraints);
        if (validationResult) {
            res.status(400).json(validationResult);
        } else {
            next()
        }
    } catch (error) {
        res.status(500).json(error);
    }

}

module.exports = queryValidate;