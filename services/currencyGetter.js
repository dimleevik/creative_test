const axios = require('axios');
const models = require('../models');

//Получение курса валют
async function getCurrency(findDate = new Date().setHours(0, 0, 0, 0)) {
    let startDate = new Date(findDate);
    let time = new Date(findDate).setDate(startDate.getDate() + 1);
    let endDate = new Date(time);
    //Ищем по указанной дате запись в БД
    let currency = await models.currency.findOne({
        date: {
            $gte: startDate,
            $lt: endDate
        }
    }).exec();
    //Если ничего не нашли, то делаем запрос на API
    let currencydata = {};
    if (!currency) {
        currencydata = await requestCurrency();
    } else {
        currencydata = currency;
    }
    return currencydata
}


async function requestCurrency() {
    let { data } = await axios.get('https://api.currencyfreaks.com/latest?apikey=8c005da8bb1f4fc091b55321572f58a0&symbols=RUB,EUR,JPY,USD')
    return await models.currency.create({ date: data.date, RUB: data.rates.RUB, EUR: data.rates.EUR, JPY: data.rates.JPY, USD: data.rates.USD },)
}

module.exports = getCurrency;