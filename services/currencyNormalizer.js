/*Функция нормализации вида отображения валют
base - Массив искомых валют, currency - Объект курса валют к доллару*/
function normalizer(base, currency) {
    let currencyArray = [];
    for (let type of base) {
        //Рассчитываем коэффициент отношения доллара к базовой валюте  
        let multier = currency['USD'] / currency[type];
        let relativeCurrency = { currency: type }
        if (base.length > 1) {
            for (let key of base) {
                //Рассчитываем курс через коэффициент и курс валюты к доллару
                if (key !== type) {
                    relativeCurrency[key] = currency[key] * multier;
                }
            }
        } else {
            //Алгоритм работы, при поиске по одному тикеру валюты 
            for (let key in currency) {
                if (key !== type) {
                    relativeCurrency[key] = currency[key] * multier;
                }
            }
        }

        currencyArray = [...currencyArray, relativeCurrency]
    }
    return currencyArray;
}
module.exports = normalizer