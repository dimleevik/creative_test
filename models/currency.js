const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const currencySchema = new Schema({
    date: { type: Date, default: new Date() },
    EUR: { type: Number },
    RUB: { type: Number },
    JPY: { type: Number },
    USD: { type: Number }
});

module.exports = mongoose.model("currency", currencySchema);
