const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const logSchema = new Schema({
    date: { type: Date, default: new Date() },
    type: { type: Object, required: true }
});

module.exports = mongoose.model("log", logSchema);
