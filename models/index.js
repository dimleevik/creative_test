const mongoose = require('mongoose');
const currency = require('./currency');
const logs = require('./logs');
mongoose.connect('mongodb://mongodb:27017/', { useNewUrlParser: true, useUnifiedTopology: true });

const db = {
    currency,
    mongoose,
    logs
}
module.exports = db