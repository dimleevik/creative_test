const date = {
    format: {
        pattern: /\d{4}-\d{2}-\d{2}/,
        flags: "i",
        message: "enter date in YYYY-MM-DD format"
    }
}

const currency = {
    currency: {
        message: 'text'
    }
}

module.exports = {
    date, currency
}