const express = require('express');
const app = express();

class httpService {
    constructor(routes) {
        app.use(routes);
    }
    start() {
        app.listen(3000, () => {
            console.log('listen on 3000...')
        })
    }
}

module.exports = httpService;