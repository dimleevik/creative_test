const redis = require("redis");

let client

class RedisService {
    connection() {
        if (!client) {
            client = redis.createClient({
                host: 'redis',
                port: '6379'
            })
        }
        return client
    }

    aget(key) {
        return new Promise((resolve, reject) => {
            this.connection().get(key, (error, res) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(res)
                }
            })
        })
    }

    aset(key, value) {
        return new Promise((resolve, reject) => {
            this.connection().set(key, value, (error, res) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(res)
                }
            })
        })
    }

    aexpire(key, ttl) {
        return new Promise((resolve, reject) => {
            this.connection().expire(key, ttl, (error, res) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(res)
                }
            })
        })
    }

    adel(key) {
        return new Promise((resolve, reject) => {
            this.connection().del(key, (error, res) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(res)
                }
            })
        })
    }

    ahdel(key, field) {
        return new Promise((resolve, reject) => {
            this.connection().hdel(key, field, (error, res) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(res)
                }
            })
        })
    }
}

module.exports = new RedisService();
