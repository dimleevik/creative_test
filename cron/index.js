const CronJob = require('cron').CronJob;
const currencyGetter = require('../services/currencyGetter');
const job = new CronJob('00 00 00 * * *', function () {
    currencyGetter();
});

module.exports = job;