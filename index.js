const httpService = require('./http');
const db = require('./models');
const router = require('./routes');
const cron = require('./cron');
const http = new httpService(router);

db.mongoose.connection.once('open', function () {
    console.log('Mongoose successfully connected')
})
cron.start();
http.start()