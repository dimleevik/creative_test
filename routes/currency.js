const Router = require('express');

const getCurrency = require('../services/currencyGetter');
const currencyNormalizer = require('../services/currencyNormalizer');
const queryValidate = require('../middlewares/queryValidate');
const { date, currency } = require('../validators');
const db = require('../models');
const authMiddleware = require('../middlewares/authMiddleware');

const router = Router();
//Роут получения курса валют
router.get('/', (req, res, next) => {
    //Пробрасываем шаблоны валидации в следующий middleware
    req.queryConstraints = { date, currency };
    next()
}, queryValidate, authMiddleware,
    async (req, res) => {
        try {
            let currency = await getCurrency(req.query.date)
            let currencyObject = {
                EUR: currency.EUR,
                RUB: currency.RUB,
                JPY: currency.JPY,
                USD: currency.USD
            }
            let requestCurrencyArray = req.query?.currency ? req.query.currency.split(',') : Object.keys(currencyObject);
            let currencyArray = currencyNormalizer(requestCurrencyArray, currencyObject);
            //Логируем запрос
            db.logs.create({ date: new Date(), type: { URL: req.url, currencies: requestCurrencyArray } });
            res.json({ date: currency.date, result: currencyArray })
        } catch (error) {
            res.status(500).json(error);
        }
    })

module.exports = router;