const Router = require('express');

const RedisService = require('../redis');
//Создание ключа api
const router = Router();
router.get('/',
    async (req, res) => {
        try {
            let randomNum = Math.random() * 100000
            let apiKey = Buffer.from(randomNum.toString()).toString('base64');
            console.log(apiKey);
            await RedisService.aset(apiKey, req.connection.remoteAddress);
            RedisService.aexpire(apiKey, 600);
            res.json({ Bearer: apiKey })
            res.end();
        } catch (error) {
            res.status(500).json(error);
        }
    })

module.exports = router;