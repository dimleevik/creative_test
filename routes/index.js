const Router = require('express');
const currency = require('./currency');
const apiKey = require('./apiKey');
const router = Router();

router.use('/currency', currency);
router.use('/apiKey', apiKey);

module.exports = router